# Detailed Explanation of the Checkout Process using DataLayer

01. The user lands on the flight search page, it fires a `product_detail_impression` [code](./dl_ee_checkout_00_productImp_detail.js)
02. The user has configure the dates and is clicking on the continue button at the end. It fires a `add-to-cart` [code](../dl_ee_addToCart.js)
03. The user lands on Personalize page. It fires the `checkout_01_personalise` [code](./dl_ee_checkout_01_personalise.js)
04. The user on the personalize page, clicks on continue. It fires the `checkout_02_seat` [code](./dl_ee_checkout_02_seats.js)
05. The user lands on Seats Page, and clicks on continue. It fires the `checkout_03_Payment` [code](./dl_ee_checkout_03_Payment.js)