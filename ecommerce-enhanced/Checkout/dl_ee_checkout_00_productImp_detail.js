window.dataLayer = window.dataLayer || [];
    dataLayer.push({
        "event" : "Product Detail",
        "page_type" : "Flight_Search",
        "ecommerce": {
            "detail": {
                "actionField" : {
                    "list" : "checkout_funnel"
                },
                "products" : [ // the first element of the array refers to the "Way" travel, the second is the "Return" travel
                    { 
                    "id" : f.DepartureCity.Code+'-'+f.ArrivalCity.Code, // *Required* combination of the airport Code in the order "Origin - Destination"
                    "name" : f.DepartureCity.Name.slice(0,10)+' - '+f.ArrivalCity.Name.slice(0,10), // combination of the cities in the order "Origin - Destination"
                    "brand" : "Air Belgium", // Should always be Air Belgium, unless other carrier
                    "variant": f.DepartureDateString.replace(/-/g,''), // Specify the travel date (use YYYYMMDD notation)
                    "flexDate" : true, // is flex data.
                    "quantity" : "4",// *Required* specify the total number of travelers for the flight. 
                    "passengerInfo" : ["adult","adult","child","infant"], // lenght = quantity but each element indicates the passenger type.
                    "flexDate" : f.UseFlexDates, // is flex data.
                },
                { // return
                    "id" : f.ArrivalCity.Code+'-'+f.DepartureCity.Code, // *Required* combination of the airport Code in the order "Origin - Destination"
                    "name" : f.ArrivalCity.Name.slice(0,10)+' - '+f.DepartureCity.Name.slice(0,10), // combination of the cities in the order "Origin - Destination"
                    "brand" : "Air Belgium", // Should always be Air Belgium, unless other carrier
                    "variant": f.ReturnDateString.replace(/-/g,''), // Specify the travel date (use YYYYMMDD notation)
                    "flexDate" : true, // is flex data.
                    "quantity" : "4",// *Required* specify the total number of travelers for the flight. 
                    "passengerInfo" : ["adult","adult","child","infant"], // lenght = quantity but each element indicates the passenger type.
                    "flexDate" : f.UseFlexDates, // is flex data.

                },
            ]
            }
        }
    })

