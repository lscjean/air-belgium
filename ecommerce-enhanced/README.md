# AirBelgium Google Analytics Ecommerce Enhanced

## Ecommerce Enhanced dataLayer Listing.

- `Product Impressions`  [dataLayer specification](./dl_ee_productImp.js)
- `Product Clicks` [dataLayer specification](./dl_ee_productCk.js)
- `Product Detail Impressions` [dataLayer specification](./Checkout/dl_ee_checkout_00_productImp_detail.js)
- `Add-to-Cart` [dataLayer specification](./dl_ee_addToCart.js)
- `Remove-to-Cart` [dataLayer specification](./dl_ee_removeToCart.js)
- `Checkout` [dataLayer specification](./Checkout)
- `Purchases` [dataLayer specification](./Checkout/dl_ee_checkout_00_productImp_detail.js)

## Introduction

In 2019, Air Belgium (AB) wishes to increase its business revenue by selling plane tickets online, hence becoming independant from Travel Agency and increase margins. 
To do this, AB mandates Mediascale to conduct their digital transformation for better performances. 
After research and investigation onto their technological stack, we noticed a very basic use of Google Analytics (GA) as well as Google Tag Manager (GTM). We also notice the benefits of using GA Ecommerce Enhanced (EE) in order to bring more clarity to the website performance. 

## Important Note for EZY Flight

Some events are not related to the booking engine. Please consider the events under Checkout, Product Update Folder as well as AddToCart & RemoveToCart


### Resources
- [Google Analytics Ecommerce Enhanced by Wizz Air](https://www.everymundo.com/resource/google-analytics-account-setup-airlines/)
- [Google Analytics EE developer tools](https://developers.google.com/tag-manager/enhanced-ecommerce)
- [AB booking engine Staging](http://test.airbelgium.ezyflight.se/en)

## EE Integration Flow
**[Detailed flow](https://bitbucket.org/lscjean/air-belgium/src/master/ecommerce-enhanced/Ecommerce%20Enhanced%20-%20Checkout.png)**

- Let's start the integration flow by navigating to the [homepage](https://www.airbelgium.com/). 

- The user scroll down and see destinations being proposed. At this stage, we will fire a `Product Impressions` ([see details](https://developers.google.com/tag-manager/enhanced-ecommerce#product-impressions))

- If the user clicks on the destination block, we will fire a `Product Click`([see details](https://developers.google.com/tag-manager/enhanced-ecommerce#product-clicks))

- Once the user land on the product page, we will fire a `Product Impressions` ([see details](https://developers.google.com/tag-manager/enhanced-ecommerce#product-impressions))

- If the user decides to jump into the Booking Section of the website, we fire nothing. 

- Once he lands on the 1st page of the booking section, we will fire a `Product Detailed Impression` ([see details](https://developers.google.com/tag-manager/enhanced-ecommerce#details))

- The user may perform flight search edit at this stage (change dates, define flight class (i.e. business, economy, etc)). These action will generate a `Product Detailed Impression` ([see details](https://developers.google.com/tag-manager/enhanced-ecommerce#details))
     
     - the user may **change its departure/return dates**, we will fire a **custom event** `product update` containing all the different information. ([see details](./product-upgrade/dl_c_changeDates.js))
     - the user may **change its departure/return Classes**, we will fire a **custom event** `product update` containing all the different information. ([see details](./product-upgrade/dl_c_changeClass.js))

- If the user is satisfied, he/she will move onto the next step by clicking on "continue", this will fire the `Add-to-cart` event ([see details](https://developers.google.com/tag-manager/enhanced-ecommerce#cart))

- the user is now navigating to the second section of the website "personnalize" and fire the `checkout` event ([see details](https://developers.google.com/tag-manager/enhanced-ecommerce#checkout))

    - the user may **add extra package to its departure/return flight**, we will fire a **custom event** `product update` containing all the different information. ([see details](./product-upgrade/dl_c_changeExtra.js))
    
- The user may want to go back to the previous page, this fires a series of event

    - generate a `Remove-to-cart` ([see details](https://developers.google.com/tag-manager/enhanced-ecommerce#cart))
    - once landed on the 1st page, generate a `Product Detailed Impression` ([see details](https://developers.google.com/tag-manager/enhanced-ecommerce#details))

- If the user decides to **click on continue**, we will fire the `checkout` event for each **validated step** except the confirmation ([see details](https://developers.google.com/tag-manager/enhanced-ecommerce#checkout))

    - for each editing of the search, we will fire a **custom event** `product update` containing all the different information. 

- once the user has finalized its purchase, he should go to the "confirmation page" hence generating a `purchase event` ([see details](https://developers.google.com/tag-manager/enhanced-ecommerce#purchases))

## DataLayer Specifications

We will follow the Google's guideline to implement such information. The dataLayer should respect the guidelines specificied at the top of the resources. 
You may find below the complete list of dataLayer used for implementing EE on AB. 

## Custom Dimensions & Metrics to take into account

`index` | `Variable_Name` | `Variable_Scope` | `Variable_Description`

### Custom Dimensions
| `index` | `Variable_Name` | `Variable_Scope` | `Variable_Description`| 
| ---| ---| ---| ---|
| 01 | flexDate | `product` | specify if the user selected flexible departure dates| 
| 02 | passengerInfo | `product` | Detail Passenger information in a Array| 
| 03 | extraWeight | `product` | Boolean to specify if user has increased its bagage weight capacity| 
| 04 | extraBaggage | `product` | Boolean to specify if user has added an extra bagage| 
| 05 | seatUpgrade | `product` | specify if the user has made a seat upgrate, and which one (specify name)| 
| 06 | hasPet | `product` | Boolean specify if the user will travel with a pet| 
| 07 | hasEquipment | `product` | Boolean specify if the user will travel sport equipment| 
| 08 | needAssistance | `product` | Boolean specify if the user will will need assistance during the travel | 
| 09 | chefMeal | `product` | Boolean Specify if the user has choosen the chefs' meal | 
| 10 | comfortKit | `product` | Boolean Specify if the user has chosen the comfort Kit | 
| 11 | Variable_Name | `Variable_Scope` | Variable_Description| 
| 12 | Variable_Name | `Variable_Scope` | Variable_Description| 
| 13 | Variable_Name | `Variable_Scope` | Variable_Description| 
| 14 | Variable_Name | `Variable_Scope` | Variable_Description| 
| 15 | Variable_Name | `Variable_Scope` | Variable_Description| 
| 16 | Variable_Name | `Variable_Scope` | Variable_Description| 
| 17 | Variable_Name | `Variable_Scope` | Variable_Description| 
| 18 | Variable_Name | `Variable_Scope` | Variable_Description| 
| 19 | Variable_Name | `Variable_Scope` | Variable_Description| 
| 20 | Variable_Name | `Variable_Scope` | Variable_Description| 

### Custom Metrics
01. | `addOnRevenue` | `product` | `specify total cost from add-on`
02. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
03. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
04. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
05. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
06. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
07. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
08. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
09. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
10. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
11. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
12. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
13. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
14. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
15. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
16. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
17. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
18. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
19. | `Variable_Name` | `Variable_Scope` | `Variable_Description`
20. | `Variable_Name` | `Variable_Scope` | `Variable_Description`