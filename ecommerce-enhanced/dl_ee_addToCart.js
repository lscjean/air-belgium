onContinue(){
    window.dataLayer = window.dataLayer || [];
    dataLayer.push({
        "event" : "Add to Cart",
        "page_type" : "Flight_Search",
        "ecommerce": {
            "currencyCode" : "EUR",
            "add": {
                "products" : [ // the first element of the array refers to the "Way" travel, the second is the "Return" travel
                    { 
                    "id" : "CRL - HKG", // *Required* combination of the airport Code in the order "Origin - Destination"
                    "name" : "Brussel - HongKong", // combination of the cities in the order "Origin - Destination"
                    "category" : "business",  // Specify if the travel class, the user has choosen
                    "brand" : "Air Belgium", // Should always be Air Belgium, unless other carrier
                    "variant": "20190525", // Specify the travel date (use YYYYMMDD notation)
                    "price" : "700", // *Required* specify the total cost of the travel
                    "quantity" : "4",// *Required* specify the total number of travelers for the flight. 
                    /** Custom dimensions */
                    "passengerInfo" : ["adult","adult","child","infant"], // Detail Passenger information in a Array
                },
                { // return
                    "id" : "HKG - CRL", // *Required*
                    "name" : "HongKong - Brussel",
                    "category" : "economy", // insurances or loans
                    "brand" : "Air Belgium",
                    "variant": "20190525",
                    "price" : "700", // *Required* is one because there's no actual cost
                    "quantity" : "4",// *Required* is one because there's no actual cost
                    /** Custom dimensions */
                    "passengerInfo" : ["adult","adult","child","infant"], // Detail Passenger information in a Array
                },
            ]
            }
        }
    })
}
