window.dataLayer = window.dataLayer || [];
    dataLayer.push({
        "event" : "Product Click",
        "page_type" : "homepage", // specify which page category
        "ecommerce": {
            "currencyCode" : "EUR",
            "click": {
                "products" : [ // the first element of the array refers to the "Way" travel, the second is the "Return" travel
                    { 
                    "id" : "CRL - HKG", // *Required* combination of the airport Code in the order "Origin - Destination"
                    "name" : "Brussel - HongKong", // combination of the cities in the order "Origin - Destination"
                    "category" : "business",  // Specify if the travel class, the user has choosen
                    "brand" : "Air Belgium", // Should always be Air Belgium, unless other carrier
                },
            ]
            }
        }
    })

